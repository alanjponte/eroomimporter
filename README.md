eroom-to-aodocs-importer
========================

A single-pass integration eRoom to AODocs importer.


Building the Executable
-----------------------

Though you can run the importer by executing `eroom-importer.py` for development,
you should execute `build.py` on a 32-bit Windows machine with the requirements
installed in order to build the binary zip.


Running the Importer
--------------------

1. Log in to Google as the AODocs storage administrator.

2. Visit AODocs, and create a new library that will hold the imported room.
   Be sure to select "Google Drive shared folders" as the library creation mode.

3. Select the new library, and then click "Administration",
   then "Administration Console" in the top right-hand side of the page.

4. Select the "Security Code" entry from the left-hand navigation bar.
 
5. If no security code exists, click the Add button to create a new one. 
   _Security codes expire after 24 hours by default, so you may want to click the
   "Permanent" button on the row associated with the new security code if you don't want
   it to expire while an import is running._

6. In eRoom, visit the room you want to import.
   Make a note of the facility and room parts of the URL of the room you want to import
   (ex. for a room URL of https://eroom2.lbl.gov/eRoom/CIS/Helpdesk/,
   the facility is CIS and the room is Helpdesk.)

7. Run the eRoom importer executable, and provide the following information when prompted:
   the base URL of the eRoom server (ex. https://eroom2.lbl.gov),
   the username and password of the eRoom user to run the import as,
   the facility and room to import,
   the AODocs security code, and
   the name of the AODocs library to import into.

8. The importer will also pop up a browser window to request OAuth approval
   to access Google Drive.
   Make sure that you're logged in as the storage administrator in the approval
   browser window -- if it opens in the wrong window, copy-paste the URL into the window
   where the storage admin is logged in.
   Grant the application approval, and return to the importer window.
   The credentials will be cached in the file "oauth-credentials.dat" for future runs.

9. Add the users who are members of the room to the new library in AODocs as viewers.


Tips and Tricks
---------------

- You can import into libraries that aren't created in "Google Drive shared folders" mode.
  If you create a custom library, the importer should just work if there is a document class
  named "Document" in the library.
  Otherwise, you'll need to change the `document_class` option in the `[aodocs]` section of
  importer.ini appropriately.
  On the other hand, the security category always needs to be named "Folder".

- You can save the eRoom password to the .ini file by creating and setting a `password`
  option in the `[eroom]` section.

- The importer can take a few command-line options instead of prompting interactively for
  input.
  Run `eroom-importer.exe --help` to get a list of options.

- Re-running the importer won't overwrite previous log files.
  Old logs will be given to a number-suffixed filename, ex. "importer.log.1".
  The importer will keep up to 10 old logs in the directory.

#this is the first README
