from datetime import datetime
import glob
import os.path
import subprocess
import zipfile


def make_timestamp():
    now = datetime.now()
    return now.strftime('%Y%m%d-%H%M%S')


if __name__ == '__main__':
    ARCHIVE = 'dist/eroom-importer_%s.zip' % make_timestamp()
    EXECUTABLES = 'dist/eroom-importer.exe'
    DATA_FILES = (
        'cacert.pem',
    )

    subprocess.call('python setup.py py2exe', shell=True)
    
    with zipfile.ZipFile(ARCHIVE, 'w') as zf:
        paths = list(DATA_FILES)
        paths.extend(glob.iglob(EXECUTABLES))

        for path in paths:
            basename = os.path.basename(path)
            zf.write(path, 'eroom-importer/%s' % basename)

    print 'Created archive %s' % ARCHIVE
