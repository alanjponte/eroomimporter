import os
import os.path


def setup():
    """Populates the environment variable to let requests know where
    the certificate bundle is.

    """

    os.environ['REQUESTS_CA_BUNDLE'] = get()


def get():
    """Return the location of the cert bundle. Invoked by httplib2."""

    return os.path.join(os.getcwd(), "cacert.pem")
