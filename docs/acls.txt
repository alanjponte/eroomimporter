One of the key differences between the permission systems in eRoom and AODocs
in permission inheritance. In eRoom, the open permissions on a file can be set
to "anyone who has access" separately from setting the edit permissions to
"anyone who can open". In AODocs, permission inheritance is combined - if a file
inherits permissions from its parent folder, it will pick up both the read and
write permissions. This isn't a problem when we're dealing with files which can
be edited and opened by anyone, but is an issue for files that can be opened by
anyone but can only be edited by a specific set of members.

In this case we have three options for the import:

1. We can copy the list of members who can open the parent folder into the ACL
   of the file. This will accurately reflect the ACLs from eRoom, though some
   flexibility will be lost (ex. if you grant someone read and write access to
   a folder which contains some read-only files that were imported from eRoom,
   you'll also need to explicitly grant them read access to the read-only
   files.)

2. We can inherit permissions from the parent folder anyway and accept that we
   will grant write permissions to users that did not previously have it in
   eRoom. This makes it more flexible (we don't need to explicitly grant access
   to every file to which a user should have read-only access when we add a user
   to a folder) though it causes access control problems.

3. We can resolve to only ever set read permissions on folders, and inheit
   unless read permissions are overridden by a child. This approach combines
   flexibility (we don't need to grant read access to new items explicitly)
   with accuracy.

The eRoom importer uses a modified version of option 3: we only set read
permissions, unless all child items have write permissions as well. Performing
imports this way requires two depth-first passes: the first pass computes an
initial set of permissions for each item, and also promotes common edit
permissions to parent items. The second pass removes edit permissions from items
that have had their edit permissions promoted to a parent item. The second pass
needs to remove promoted permissions from child items before removing its own
promoted permissions, or else promoted permissions will not be removed at every
second level.

In order to cut down on memory use, the ACL importing code persists ACLs to disk
when they are not in use. This cuts memory use down to (approprimately) the
logarithm of the size of a room, and helps deal with large rooms with lots of
implicit permissions.
