from datetime import datetime
import logging
import os.path
import pdb
import sys

import ca_certs_locater
from eroom_importer import aodocs
from eroom_importer import config
from eroom_importer import eroom
from eroom_importer import logging_config
from eroom_importer import mediator


logger = logging.getLogger(__name__)


def handle_exception(type_, value, tb):
    if not isinstance(value, Exception):
        return  ''' Don't show a debugger on KeyboardInterrupts, etc.'''

    logger.critical("An uncaught exception occurred:\n",
                    exc_info=(type_, value, tb))
    print
    print "=== DEBUGGER ENTERED: use the command 'quit' to exit ======"
    print
    pdb.pm()


sys.excepthook = handle_exception

logging_config.setup('logging.json')

ca_certs_locater.setup()

configuration = config.Config()

# TODO: Push this into the eRoom client
eroom_url = '%s/eRoomXML' % configuration.eroom_url.rstrip('/')
client = eroom.EroomClient(
    eroom_url,
    username=configuration.eroom_username,
    password=configuration.eroom_password,
)

if configuration.list:
    rooms = sorted(client.list_rooms())
    for facility_urlname, room_urlname, room_name in rooms:
        print ('[%s:%s] %s' %
               (facility_urlname, room_urlname, room_name))

    sys.exit(0)

room_handle = eroom.EroomRoomHandle(
    client,
    configuration.facility,
    configuration.room,
)

http = aodocs.get_authorized_http('oauth-credentials.dat')

library_handle = aodocs.AodocsLibraryHandle(
    http,
    configuration.aodocs_url,
    configuration.aodocs_security_code,
    configuration.library_name,
)

cache_directory = os.path.join(
    configuration.importer_cache_directory,
    configuration.facility,
    configuration.room,
)

client = mediator.ImportClient(cache_directory, room_handle, library_handle,
                               configuration.aodocs_document_class,
                               configuration.aodocs_email_document_class,
                               configuration.eroom_type)

if configuration.dirty:
    configuration.write_ini_config()

start_time = datetime.now()
mediator.do_import(client)
end_time = datetime.now()

elapsed_time = end_time - start_time
elapsed_seconds = elapsed_time.total_seconds()

logger.info('Finished successfully in %s', elapsed_time)
logger.info('Processed %d folders (%.1f folders / second)',
            client.folder_count, client.folder_count / elapsed_seconds)
logger.info('Processed %d files (%.1f files / second)',
            client.file_count, client.file_count / elapsed_seconds)
logger.info('Processed %d MB (%.1f KB / second)',
            client.file_byte_count / 1024 / 1024,
            client.file_byte_count / 1024 / elapsed_seconds)

if configuration.cleanup:
    client.cleanup()
