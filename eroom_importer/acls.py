from . import eroom


class EveryoneScope(object):
    """An ACL scope that includes all users with access to the item.
    If this scope controls viewers, then we will just inherit the ACL
    from the parent; if we're managing editors, we'll need to copy the
    ACLs explicitly.

    """

    should_inherit_acls = True

    def __init__(self, emails):
        self.emails = frozenset(emails)

    @property
    def viewers(self):
        # In the case of everyone, we'll inherit the ACLs, so we shouldn't
        # to list them explicitly in the list we send to AODocs.
        return frozenset()

    @property
    def editors(self):
        return self.emails

    def __str__(self):
        return 'everyone(%s)' % self.emails


class ListScope(object):
    """An ACL scope that only includes a specific set of users. Users
    must be listed explicitly whether this is a view or an edit scope.

    """

    should_inherit_acls = False

    def __init__(self, emails):
        self.emails = frozenset(emails)

    @property
    def viewers(self):
        return self.emails

    @property
    def editors(self):
        return self.emails

    def __str__(self):
        return 'list(%s)' % self.emails


def make_scope(scope_type, emails, parent_scope):
    """Create an ACL scope instance given an eRoom scope type, a list of
    emails associated with the scope in eRoom, and the ACL scope of
    the parent item. The emails associated with the new scope will be
    a subset of the emails of the parent scope.

    """

    if scope_type == 'erScopeAnyone':
        return EveryoneScope(parent_scope.emails)
    elif scope_type == 'erScopeList':
        return ListScope(frozenset(emails) & parent_scope.emails)
    elif scope_type == 'erScopeCoordinatorsOnly':
        return ListScope(frozenset())
    else:
        raise RuntimeError('Unknown scope: "%s"' % scope_type)


class Acl(object):
    """An Acl object represents the access controls on a specific item,
    ex. a file, folder, or an inbox. Acls indicate whether permissions
    should be inherited from the parent item, and which email addresses
    should be listed as readers or writers on the request sent to AODocs.

    """

    def __init__(self, open_scope, edit_scope):
        self.open_scope = open_scope
        self.edit_scope = edit_scope

    @property
    def should_inherit_acls(self):
        return self.open_scope.should_inherit_acls

    @property
    def viewers(self):
        return self.open_scope.viewers - self.edit_scope.editors

    @property
    def editors(self):
        return self.edit_scope.editors

    @editors.setter
    def editors(self, emails):
        editor_emails = frozenset(emails) & self.open_scope.editors
        self.edit_scope = ListScope(editor_emails)

    def remove_redundant_editors(self, parent):
        """Removes inherited editors that are listed in the parent Acl."""
        if self.should_inherit_acls:
            self.editors = self.editors - parent.editors

    @staticmethod
    def from_xml_element(parent, element):
        open_scope_type = eroom.findtext(
            element, './er:AccessControl/er:OpenAccessScope')
        edit_scope_type = eroom.findtext(
            element, './er:AccessControl/er:EditAccessScope')

        # eRoom may include nested members on an item, ex. concrete users
        # nested in a custom role or group, possibly recursively. Only
        # concrete users have Email tags, so we extract Email tags recursively
        # to expand all users from all levels of the ACL.
        openers = eroom.findtexts(
            element,
            './er:AccessControl/er:OpenAccessList//er:Member/er:Email')
        editors = eroom.findtexts(
            element,
            './er:AccessControl/er:EditAccessList//er:Member/er:Email')

        open_scope = make_scope(open_scope_type, openers, parent.open_scope)
        edit_scope = make_scope(edit_scope_type, editors, open_scope)

        return Acl(open_scope, edit_scope)
