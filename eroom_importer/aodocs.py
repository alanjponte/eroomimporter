"""
This package implements an import interface to AODocs, using their public
RESTful API. It includes mapping problematic MIME types, managing the OAuth
flow in a user-friendly manner, performing uploads to Google Drive, and sending
data to AODocs for files and folders.
"""

from datetime import datetime
import json
import logging
import os.path

from apiclient.discovery import build
from apiclient.http import MediaFileUpload
import httplib2
import mimetypes
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.file import Storage as FileStorage
import oauth2client.tools
import requests

from retry import FinalError, retry


CLIENT_ID = '478267669712-l2q23oktriqek30pv58q5b74mm80ab69.apps.googleusercontent.com'
CLIENT_SECRET = '_6DX5TDmNiSsECBHgJWVaoFl'
OAUTH_SCOPE = 'https://www.googleapis.com/auth/drive'
REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'
 

'''CLIENT_ID = '144381487363.apps.googleusercontent.com'
CLIENT_SECRET = 'wXLi1viMRhngOiwmeyNcxmxt'
OAUTH_SCOPE = 'https://www.googleapis.com/auth/drive'
REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'
'''


logger = logging.getLogger(__name__)


# Map from file extensions to MIME types for some MIME types that are not
# detected properly by the Google Drive API
EXTENSION_MIME_MAP = {
    '.xls': 'application/vnd.ms-excel',
    '.doc': 'application/msword',
    '.ppt': 'application/vnd.ms-powerpoint',
    '.vsd': 'application/vnd.ms-visio',
    '.docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    '.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    '.pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
}


def get_mime_type(path):
    """Return an appropriate MIME type for the file at `path`. The returned
    MIME type will always be a string, not None.

    """

    _, extension = os.path.splitext(path)
    mime_type = EXTENSION_MIME_MAP.get(extension, None)
    if mime_type is None:
        mime_type, _ = mimetypes.guess_type(path)
    if mime_type is None:
        # Sometimes we can't guess the the type. If it can't be guessed, than
        # the Google Drive python library will abort without uploading the
        # file. Setting the type to a sane default works around the problem.
        mime_type = 'application/octet-stream'
    return mime_type


def format_datetime(dt):
    """Convert a `datetime.datetime` to an AODocs-compatible timestamp."""
    return dt.strftime('%b %d, %Y %I:%M:%S %p')


def format_acl(readers, writers):
    """Creates an AODocs-style ACL given a list of users who have read
    access to a resource, and a list of users who have write access to
    a resource. The two lists should be disjoint -- if a user was both
    read a write access, then they should only be listed in the writers
    list.

    """

    return ([{'value': email, 'right': 'READ'} for email in readers] +
            [{'value': email, 'right': 'WRITE'} for email in writers])


class AodocsConfigurationError(FinalError):
    pass


class AodocsLibraryHandle:
    """An AodocsLibraryHandle represents a connection to AODocs which
    is bound to a specific library. It also encapsulates the ability
    to upload a file to drive before adding it to AODocs.

    """

    def __init__(self, http, endpoint, security_code, library_name):
        """Create a new AodocsLibraryHandle.

        :param http: an httplib2.Http instance which has been authorized
            via OAuth for the drive scope.
        :param endpoint: the base URL of the AODocs server to connect to
        :param security_code: the security code used to authenticate with
            AOdocs.
        :param library_name: the name of the library to import items into

        """

        self.drive_service = build('drive', 'v2', http=http)
        self.endpoint = endpoint.rstrip('/')
        self.security_code = security_code
        self.library_name = library_name

        self.folder_id = self.get_folder_id()

    @retry
    def perform_drive_upload(self, body, media_body=None):
        endpoint = self.drive_service.files()
        # TODO: Handle 500 / 501 / 502 / 502
        handle = endpoint.insert(body=body, media_body=media_body).execute()
        return handle

    def upload_file_to_drive(self, path, title=None):
        """Upload a file on disk to Google Drive.

        :param path: the path to the file on disk
        :param title: the filename to present in Google Drive. If not
            specified, the basename of the path is used instead.

        """
        start_time = datetime.now()
        file_size = os.path.getsize(path)

        if title is None:
            title = os.path.basename(path)

        mime_type = get_mime_type(path)

        if file_size == 0:
            media_body = None
        else:
            media_body = MediaFileUpload(
                path, mimetype=mime_type, resumable=True)

        body = {
            'title': title,
            'mimeType': mime_type,
            'parents': [{'id': self.folder_id}],
        }

        handle = self.perform_drive_upload(body, media_body)
        file_id = handle['id']

        elapsed_time = datetime.now() - start_time
        logger.info('Uploaded %s (%d KB) in %s (%d KB / s)',
                    path, file_size / 1024, elapsed_time,
                    file_size / 1024 / elapsed_time.total_seconds())

        return file_id

    @retry
    def make_aodocs_request(self, url_suffix, payload):
        """Send a request to AODocs.

        :param url_suffix: The path part of the URL to make the request
            to, ex. `/import/createDocument`.
        :param payload: the POST body payload making up the request.

        """

        if not url_suffix.startswith('/'):
            raise ValueError('URL suffix must begin with a /')

        url = '%s%s' % (self.endpoint, url_suffix)

        logger.debug('Sending request to %s:\n%r', url, payload)
        response = requests.post(url, data=payload)
        logger.debug('Received response %s:\n%s',
                     response.status_code, response.text)

        if 400 <= response.status_code < 500:
            message = response.json().get('message', 'Unknown error')
            raise AodocsConfigurationError(message)

        response.raise_for_status()
        return response

    def get_folder_id(self):
        """Fetch the resource ID of the library folder from AODocs.
        All files should be uploaded to the library folder before being
        added in AODocs.

        """

        request = {
            'securityCode': self.security_code,
            'libraryName': self.library_name,
        }

        response = self.make_aodocs_request('/import/library', request)
        values = response.json()
        folder_id = values['resourceId']

        return folder_id

    def create_document(self, document):
        """Create a new document in the AODocs library.

        :param document: a dict describing the document according to the AODocs
            REST interface. The `libraryName` field may be omitted from the
            document, as it will be filled in automatically.

        """

        document['libraryName'] = self.library_name
        request = {
            'securityCode': self.security_code,
            'document': json.dumps(document),
        }

        self.make_aodocs_request('/import/createDocument', request)

    def create_folder(self, class_name, folder):
        """Create a new folder in the AODocs library.

        :param class_name: the name of the document class to associate the
            folder with.
        :param folder: a dict describing the folder according to the AODocs
            REST interface.

        """

        request = {
            'securityCode': self.security_code,
            'libraryName': self.library_name,
            'className': class_name,
            'folder': json.dumps(folder),
        }

        self.make_aodocs_request('/import/createSecurityCategory', request)


def get_authorized_http(credential_filename):
    """Create a httplib2.Http instance authorized for the Drive scope.

    :param credential_filename: path to a credential file to load credentials
        from and save credentials to. If credentials can't be loaded from the
        file, then the user's browser will be sent to an OAuth approval page
        to request access.

    """

    storage = FileStorage(credential_filename)
    credentials = storage.get()

    if credentials is None:
        flow = OAuth2WebServerFlow(CLIENT_ID, CLIENT_SECRET,
                                   OAUTH_SCOPE, REDIRECT_URI)

        # TODO: Use run_flow when the next version of oauth2client is released.
        credentials = oauth2client.tools.run(flow, storage)

    credentials.set_store(storage)
    http = httplib2.Http()
    http = credentials.authorize(http)
    return http


if __name__ == '__main__':
    logging.basicConfig(
        format='%(asctime)-15s %(levelname)-8s %(name)s %(funcName)s L%(lineno)s %(message)s',
        level=logging.DEBUG,
    )

    AODOCS_ENDPOINT = 'https://ao-docs.appspot.com'
    SECURITY_CODE = '000001412C22793CADB93F6FDD127DC9'
    LIBRARY_NAME = 'import-test-2'

    http = get_authorized_http('oauth-credentials.dat')
    library_handle = AodocsLibraryHandle(
        http, AODOCS_ENDPOINT, SECURITY_CODE, LIBRARY_NAME)

    file_id = library_handle.upload_file_to_drive('eroom_importer/test_upload_speed.py')

    document = {
        'className': 'Document',
        'title': 'test_upload_speed.py',
        'richText': None,
        'fields': {
        },
        'categories': {
            'Folder': [['root', 'a', 'sub/1']],
        },
        'attachments': [file_id],
        'pictures': [],
        'version': None,
        'state': None,
        'initialAuthor': None,
        'creationDate': None,
        'updateAuthor': None,
        'modificationDate': None,
        'acl': [],
        'applyContextACL': True,
    }

    root_folder = {
        'folderNames': ['root'],
        'acl': [
            {
                'value': 'user1@test1.sheepdoginc.ca',
                'right': 'WRITE',
            },
        ],
        'applyContextACL': False,
    }

    sub_folder = {
        'folderNames': ['root', 'a'],
        'acl': [],
        'applyContextACL': False,
    }

    library_handle.create_document(document)
    library_handle.create_folder('Document', root_folder)
    library_handle.create_folder('Document', sub_folder)
