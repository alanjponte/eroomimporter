import argparse
from ConfigParser import RawConfigParser
import getpass
import logging
import os.path


logger = logging.getLogger(__name__)


def read_command_line():
    parser = argparse.ArgumentParser(description='Import an eRoom to AODocs')
    parser.add_argument('--list', action='store_true',
                        help='list available rooms and terminate')
    parser.add_argument('--cleanup', action='store_true',
                        help='clean up cache after finishing')
    parser.add_argument('--config', dest='config_path', default='importer.ini',
                        help='the path to the configuration file')
    parser.add_argument('--facility', dest='facility',
                        help='the facility urlname to import from')
    parser.add_argument('--room', dest='room',
                        help='the room urlname to import from')
    parser.add_argument('--library-name', dest='library_name',
                        help='the library name to import into')

    args = parser.parse_args()
    return args


class Config:
    def __init__(self):
        self.args = read_command_line()
        self.ini_config = RawConfigParser()
        self.dirty = False

        if os.path.exists(self.args.config_path):
            self.ini_config.read([self.args.config_path])

    def set_ini_option(self, section, option, value):
        if not self.ini_config.has_section(section):
            self.ini_config.add_section(section)

        self.ini_config.set(section, option, value)
        self.dirty = True

    def get_ini_option(self, section, option, default=None, prompt=None):
        if prompt is None:
            full_prompt = '%s %s: ' % (section, option)
        else:
            full_prompt = '%s: ' % prompt

        if not self.ini_config.has_option(section, option):
            if default:
                value = default
            else:
                value = raw_input(full_prompt)

            self.set_ini_option(section, option, value)

        return self.ini_config.get(section, option)

    def write_ini_config(self):
        with open(self.args.config_path, 'w') as f:
            self.ini_config.write(f)

    def get_argument(self, option, prompt):
        value = getattr(self.args, option, None)

        if value is None:
            full_prompt = '%s: ' % prompt
            value = raw_input(full_prompt)
            setattr(self.args, option, value)

        return value

    @property
    def list(self):
        return self.args.list

    @property
    def cleanup(self):
        return self.args.cleanup is True

    @property
    def facility(self):
        return self.get_argument('facility', 'eRoom facility')

    @property
    def room(self):
        return self.get_argument('room', 'eRoom room (from url)')

    @property
    def library_name(self):
        return self.get_argument('library_name', 'AoDocs Library name')

    @property
    def aodocs_url(self):
        return self.get_ini_option('aodocs', 'url',
                                   default='https://ao-docs.appspot.com')

    @property
    def aodocs_security_code(self):
        return self.get_ini_option('aodocs', 'security_code',
                                   prompt='AODocs security code')

    @property
    def aodocs_document_class(self):
        return self.get_ini_option('aodocs', 'document_class',
                                   default='Document')

    @property
    def aodocs_email_document_class(self):
        return self.get_ini_option('aodocs', 'email_document_class',
                                   default='Email')

    @property
    def eroom_url(self):
        return self.get_ini_option('eroom', 'url', prompt='eRoom URL')

    @property
    def eroom_username(self):
        return self.get_ini_option('eroom', 'username',
                                   prompt='eRoom username')

    @property
    def eroom_password(self):
        if not hasattr(self, '_eroom_password'):
            if self.ini_config.has_option('eroom', 'password'):
                self._eroom_password = self.ini_config.get('eroom', 'password')
            else:
                self._eroom_password = getpass.getpass('eRoom password: ')

        return self._eroom_password

    @property
    def eroom_type(self):
        return self.get_ini_option('eroom', 'type', default=None)

    @property
    def importer_cache_directory(self):
        return self.get_ini_option('importer', 'cache_directory',
                                   default='eroom-cache')
