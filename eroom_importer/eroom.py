import base64
import logging
import re
import xml.sax
import xml.sax.handler


import iso8601
from lxml import etree
from lxml.builder import ElementMaker
import requests

import utils
from retry import retry


__all__ = ('EroomClient', 'EroomRoomHandle', 'find', 'findall', 'findtext',)


logger = logging.getLogger(__name__)


NAMESPACES = {
    'er': 'http://www.eroom.com/eRoomXML/2003/700',
    'soap': 'http://schemas.xmlsoap.org/soap/envelope/',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
}


soap = ElementMaker(namespace=NAMESPACES['soap'], nsmap=NAMESPACES)
er = ElementMaker(namespace=NAMESPACES['er'], nsmap=NAMESPACES)


def make_member_attributes(max_depth=5):
    attributes = [
        er.Name(),
        er.Email(),
        er.ID(),
    ]

    if max_depth > 0:
        child_attributes = make_member_attributes(max_depth - 1)
        attributes.append(er.Members(er.Member(*child_attributes)))

    return attributes


def make_members_attributes():
    return [er.Member(*make_member_attributes())]


ItemType = er.Item(
    # Properties of all items
    er.ID(),
    er.ParentID(),
    er.Name(),
    er.IsContainer(),

    er.CreateDate(),
    er.Creator(
        er.Email(),
    ),

    er.ModifyDate(),
    er.Modifier(
        er.Email(),
    ),

    # Properties of the subset of items we care about
    er.Path(),
    er.URL(),
    er.RelativeURL(),
    er.AccessControl(
        er.OpenAccessScope(),
        er.OpenAccessList(*make_members_attributes()),
        er.EditAccessScope(),
        er.EditAccessList(*make_members_attributes()),
    ),

    # erItemTypeFolder, erItemTypeCalendar, etc.
    er.Description(
        er.HTML(),
    ),

    # erItemTypeFile
    er.BaseName(),
    er.Extension(),
    er.FileURL(),

    # erItemTypeInboxPage
    er.EmailAddress(),
    er.InboxAddress(),
)


def find(element, match):
    return element.find(match, namespaces=NAMESPACES)


def findtext(element, match, default=None):
    return element.findtext(match, default=default, namespaces=NAMESPACES)


def findall(element, match):
    return element.findall(match, namespaces=NAMESPACES)


def findtexts(root, xpath):
    elements = findall(root, xpath)
    return [element.text for element in elements]


def get_xsi_type(element):
    type_ = element.attrib['{http://www.w3.org/2001/XMLSchema-instance}type']
    return type_.split(':')[-1]  # Pull the out the namespace from the value


FOLDER_SPLIT_REGEX = re.compile(r'(?<=[^\\])/')
FOLDER_ONE_ESCAPE_REGEX = re.compile(r'([^\\])\\([^\\])')


def get_path(path):
    """Convert a forward-slash separated path in eRoom into a list of path
    components to be used in AODocs.

    """

    path = path.lstrip('/')
    parts = re.split(FOLDER_SPLIT_REGEX, path)
    parts = [re.sub(FOLDER_ONE_ESCAPE_REGEX, r'\1\2', part) for part in parts]
    parts = [part.replace('\\\\', '\\') for part in parts]
    return parts


def dirname(path):
    return get_path(path)[:-1]


def basename(path):
    return get_path(path)[-1]


def parse_datetime(s):
    return iso8601.parse_date(s)


class EroomClient:
    """Implements a SOAP client for eRoom. This client implements
    its own logic to send and receive SOAP messages, as eRoom omits
    all of its complex elements types from the WSDL. Furthermore, all of
    the eRoom documentation deals with the complex types as XML elements,
    not SOAP types, which makes it difficult to map from eRoom examples
    to concrete requests. Instead, the EroomClient takes etree Elements
    as arguments to requests, and returns Elements as responses.

    """

    def __init__(self, url, username=None, password=None):
        self.url = url
        self.username = username
        self.password = password

    def execute_xml_command(self, command, stream=False):
        """Execute an XML command on the eRoom server, and return
        the underlying `requests` response in a mode depending on the
        value of the `stream` parameter. No response logging or
        manipulation is performed by this method.

        """

        headers = {
            'Content-Type': 'application/soap+xml; charset=utf-8',
        }

        envelope = soap.Envelope(
            soap.Body(
                er.ExecuteXMLCommand(
                    er.eRoomXML(
                        command
                    )
                )
            )
        )

        payload = etree.tostring(envelope, pretty_print=True)
        logger.debug('Sending request to %s:\n%s', self.url, payload)

        response = requests.post(
            url=self.url,
            verify=False,
            stream=stream,
            auth=(self.username, self.password),
            headers=headers,
            data=payload,
        )

        return response

    def execute_streaming_xml_command(self, command):
        """Execute an XML command on the eRoom server, and return
        the underlying `requests` response in streaming mode.

        This method does not automatically retry on failure, as failures
        may occur while reading the streaming content. Clients should
        be decorated with @retry instead.

        """
        response = self.execute_xml_command(command, stream=True)
        logger.debug('Received headers: %s', response.status_code)

        response.raise_for_status()
        return response

    @retry
    def execute_sync_xml_command(self, command):
        """Execute an XML command on the eRoom server, and return the
        response. The command should be an `er:command` element, while
        returned response will be an `er:response` element.

        """

        response = self.execute_xml_command(command, stream=False)
        logger.debug('Received response %s:\n%s',
                     response.status_code, response.text)

        response.raise_for_status()
        document = etree.fromstring(response.content)
        return find(document, './/er:eRoomXML/er:response')

    def getproperties(self, select, prototype):
        """Fetch data from eRoom by matching XML elements against
        the XPath `select`, and returning an XML structure matching
        the element tree `prototype`. The container for the returned
        elements will be the leaf element(s) of `select`.

        """

        command = er.command(
            er.getproperties(
                prototype,
            ),
            select=select,
        )

        response = self.execute_sync_xml_command(command)
        return response

    def list_rooms(self):
        """Return a list representing all of the rooms on the
        eRoom server. The each element in the list is a
        `(facility_urlname, room_urlname, room_name)` tuple,
        where `facility_urlname` and `room_urlname` are the facility
        and room parts of the URL to access the room, and room_name is
        the human-readable name for the room presented in the front end.

        """

        response = self.getproperties(
            'Facilities',
            er.Facility(
                er.URLName(),
                er.Rooms(
                    er.Room(
                        er.DisplayName(),
                        er.URLName(),
                    )
                )
            )
        )

        names = []

        for facility in findall(response, './/er:Facility'):
            facility_urlname = findtext(facility, './er:URLName')
            for room in findall(facility, './/er:Room'):
                room_name = findtext(room, './er:DisplayName')
                room_urlname = findtext(room, './er:URLName')
                names.append((facility_urlname, room_urlname, room_name))

        return names


class DownloadHandler(xml.sax.handler.ContentHandler):
    """SAX content handler used to generate a raw bytestream from
    an eRoom file content response. As base64-encoded text is parsed,
    it is added to a content buffer which the client can pull from by
    calling `pop_content()`.

    This handler type is designed to be used with an IncrementalParser
    implementation. If a non-streaming parser is used, memory may be
    exhausted.

    """

    def __init__(self):
        self.is_in_file_contents = False
        self.content = []

    def pop_content(self):
        """Fetch the latest decoded content from the handler. This
        method returns a byte string, though the string may be empty.

        """

        content = ''.join(self.content).replace('\n', '')
        content_length = len(content)
        remainder = content_length % 4
        block_length = content_length - remainder
        block = content[:block_length]
        self.content = [content[block_length:]] if remainder else []

        byte_string = base64.standard_b64decode(block)
        return byte_string

    def startElement(self, name, attrs):
        if name == 'er:FileContents':
            self.is_in_file_contents = True

    def endElement(self, name):
        if name == 'er:FileContents':
            self.is_in_file_contents = False

    def characters(self, content):
        if self.is_in_file_contents:
            self.content.append(content)


class EroomRoomHandle:
    """An EroomRoomHandle represents a client for a specific room
    in eRoom. Many of the queries we want to make against eRoom
    (ex. list root items, list child items, and fetch the file content
    for an item) are logically limited to a specific room. Instead
    of forcing callers to pass a facility and room to each invokation
    of a method on an EroomClient, the EroomRoomHandle allows users
    to pass a single handle encapsulating a client, facility, and room.

    """

    def __init__(self, client, facility_urlname, room_urlname):
        self.client = client
        self.facility_urlname = facility_urlname
        self.room_urlname = room_urlname

    @property
    def room_xpath(self):
        query = (
            "Facilities/Facility[URLName='{self.facility_urlname}']/"
            "Rooms/Room[URLName='{self.room_urlname}']"
        ).format(self=self)

        return query

    def list_members(self):
        """Fetch the list of members of the room."""

        query = '{self.room_xpath}'.format(self=self)
        response = self.client.getproperties(
            query,
            er.Members(*make_members_attributes())
        )

        members = find(response, './/er:Members')
        return members

    def list_items(self, parent_item_id=None):
        """Fetch the list of child items of the parent with the specified
        ID. If the parent's ID is not specified, then the top-level items
        on the room's home page are returned instead.

        """

        # TODO: Need a better name than "base_query"
        if parent_item_id is None:
            base_query = "{self.room_xpath}/HomePage/Items"
        else:
            base_query = "{self.room_xpath}//Item[ID='{parent_item_id}']/Items"

        query = base_query.format(self=self, parent_item_id=parent_item_id)
        response = self.client.getproperties(query, ItemType)
        items = find(response, './er:Items')

        return items

    @retry
    def download_item(self, item_id, chunk_size=1024 * 1024):
        """Fetch the content associated with an item of type
        erItemTypeFile. The content is returned as a (binary) string.

        """

        base_query = "{self.room_xpath}//Item[ID='{item_id}']"
        query = base_query.format(self=self, item_id=item_id)

        command = er.command(
            er.getproperties(
                er.FileContents(),
            ),
            select=query,
        )

        response = self.client.execute_streaming_xml_command(command)

        content_handler = DownloadHandler()
        parser = xml.sax.make_parser(['xml.sax.expatreader'])
        parser.setContentHandler(content_handler)

        for chunk in response.iter_content(chunk_size=chunk_size):
            parser.feed(chunk)
            byte_string = content_handler.pop_content()
            if byte_string:
                yield byte_string


if __name__ == '__main__':
    import ca_certs_locater
    ca_certs_locater.setup()

    USERNAME = 'aponte'
    PASSWORD = '@graphThe0ry'
    #URL = "https://lbnl131.lbl.gov/eRoom/aodocs/TEAM"
    URL = "https://lbl131.lbl.gov/eRoom/aodocs/B51Demo"

    logging.basicConfig(level=logging.DEBUG)

    client = EroomClient(URL, username=USERNAME, password=PASSWORD)
    room_handle = EroomRoomHandle(client, 'IT', 'Xythos')
    #room_handle.get_members()
    room_handle.list_members()
