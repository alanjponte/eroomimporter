import logging
import logging.handlers
import json
import os.path

from . import utils


# Aside from the two references to `False`, the following dict literal is also
# valid JSON. We can paste it into a JSON logging configuration if we need
# any custom configuration to help LBL with debugging.

DEFAULT_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "incremental": False,
    "formatters": {
        "status": {
            "format": "%(asctime)-15s %(levelname)s %(message)s"
        },
        "full": {
            "format": "%(asctime)-15s %(levelname)-8s %(name)s %(funcName)s L%(lineno)s %(message)s"
        }
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": "status"
        },
        "file": {
            "level": "DEBUG",
            "class": "eroom_importer.logging_config.RotateAtStartHandler",
            "filename": "logs/importer.log",
            "backupCount": 10,
            "formatter": "full"
        }
    },
    "root": {
        "level": "DEBUG",
        "handlers": ["console", "file"]
    },
    "loggers": {
        "apiclient.discovery": {
            "level": "WARNING"
        },
        "oauth2client.util": {
            "level": "ERROR"
        },
        "requests": {
            "level": "WARNING"
        }
    }
}


class RotateAtStartHandler(logging.handlers.RotatingFileHandler):
    def __init__(self, filename, mode='a', backupCount=0, encoding=None):
        utils.ensure_directory_exists(filename)

        super(RotateAtStartHandler, self).__init__(
            filename,
            mode=mode,
            backupCount=backupCount,
            encoding=encoding,
            delay=True,
        )

        if os.path.exists(filename):
            self.doRollover()


def setup(logging_config_filename):
    if os.path.exists(logging_config_filename):
        with open(logging_config_filename, 'rb') as f:
            config = json.load(f)
    else:
        config = DEFAULT_CONFIG

    logging.config.dictConfig(config)
