from xml.sax.saxutils import escape

import cPickle as pickle
from datetime import datetime
import logging
import logging.config
import os
import os.path

from . import acls
from . import aodocs
from . import eroom
from . import utils
from .retry import retry


logger = logging.getLogger(__name__)


class ImportClient:
    def __init__(self, path, room_handle, library_handle, document_class,
                 email_document_class, only_type=None):
        self.path = path
        self.room_handle = room_handle
        self.library_handle = library_handle
        self.document_class = document_class
        self.email_document_class = email_document_class

        self.document_map_path = self.get_cache_path('document.map')
        self.document_map = utils.AppendableMap(self.document_map_path)

        self.attachment_map_path = self.get_cache_path('attachment.map')
        self.attachment_map = utils.AppendableMap(self.attachment_map_path)

        self.only_type = only_type

        self.file_count = 0
        self.file_byte_count = 0
        self.folder_count = 0

    def close(self):
        self.document_map.close()
        self.attachment_map.close()

    def cleanup(self):
        self.close()
        os.remove(self.document_map_path)
        os.remove(self.attachment_map_path)

    def get_cache_path(self, *parts):
        all_parts = (self.path,) + parts
        path = os.path.join(*all_parts)
        utils.ensure_directory_exists(path)
        return path

    # eRoom client methods:

    def list_eroom_members(self):
        return self.room_handle.list_members()

    def list_eroom_items(self, parent_item_id=None):
        return self.room_handle.list_items(parent_item_id)

    @retry
    def download_eroom_item(self, item_id, filename):
        item_file_path = self.get_cache_path('files', filename)

        if not os.path.exists(item_file_path):
            try:
                with open(item_file_path, 'wb') as f:
                    for chunk in self.room_handle.download_item(item_id):
                        f.write(chunk)
            except:
                # If the download fails, remove the file from the cache
                os.remove(item_file_path)
                raise

        return item_file_path

    # AODocs client methods:

    def upload_aodocs_file(self, item_id, filename, title=None):
        self.file_byte_count += os.path.getsize(filename)

        if not filename in self.attachment_map:
            attachment_id = self.library_handle.upload_file_to_drive(
                filename, title=title)
            self.attachment_map[filename] = attachment_id

        return self.attachment_map[filename]

    def create_aodocs_document(self, item_id, document):
        self.file_count += 1
        return self.library_handle.create_document(document)

    def create_aodocs_folder(self, item_id, class_name, folder):
        self.folder_count += 1
        return self.library_handle.create_folder(class_name, folder)

    # Mediator state persistence methods

    def has_imported_document(self, item_id):
        return item_id in self.document_map

    def record_imported_document(self, item_id):
        timestamp = datetime.now().isoformat()
        self.document_map[item_id] = timestamp

    # On-disk ACL persistence

    def get_acl_cache_path(self, uid):
        basename = '%s.dat' % uid
        path = self.get_cache_path('acls', uid[-1], basename)
        return path

    def store_acl(self, uid, acl):
        path = self.get_acl_cache_path(uid)
        with open(path, 'wb') as f:
            pickle.dump(acl, f)

    def load_acl(self, uid):
        path = self.get_acl_cache_path(uid)
        with open(path, 'rb') as f:
            return pickle.load(f)


def translate_timestamp(s):
    dt = eroom.parse_datetime(s)
    return aodocs.format_datetime(dt)


ER_DOCUMENT = 'Document'
ER_EMAIL = 'Email'

handlers = {}
handlers[ER_DOCUMENT] = {}
handlers[ER_EMAIL] = {}


def get_handler(eroom_type):
    return handlers[ER_DOCUMENT].get(
        eroom_type,
        handlers[ER_EMAIL].get(eroom_type, None))


def handler_for(internal_document_class, eroom_type):
    """
    Data Model:
    https://irmm.jrc.ec.europa.eu/collaboration/eRoomHelp/en/XML_Help/xml.htm

    Args:
        internal_document_class: (Email | Document) -
            internal representations of the document classes being used. The
            system may be configured to use other document types in aodocs,
            but the ones here map to the main types coming OUT of eRoom.

        eroom_type: (erItemTypeFolderPage | erItemTypeFile |
                     erItemTypeMailPage | erItemTypeInboxPage) -
            the eRoom API representation of an element type.
    """
    def decorator(f):
        handlers[internal_document_class][eroom_type] = f
        return f
    return decorator


def run_handler(element, parent_acl, client):
    eroom_type = eroom.get_xsi_type(element)

    handler = get_handler(eroom_type)
    if handler is not None:
        acl = handler(element, parent_acl, client)
    else:
        acl = None

    return acl


@handler_for(ER_EMAIL, 'erItemTypeInboxPage')
def handle_inbox(element, parent_acl, client):
    """
    The inbox folder which houses all the email messages, should be handled
    simlilarly to eroom folders, but use the correct document type in aodocs.
    """

    # do not import emails if we're importing documents.
    if client.only_type and client.only_type != ER_EMAIL:
        return

    uid = eroom.findtext(element, './er:ID')
    path = eroom.findtext(element, './er:Path')
    acl = client.load_acl(uid)
    logger.info('Handling Inbox Folder %s:', path)

    child_elements = client.list_eroom_items(uid)
    for child_element in child_elements:
        run_handler(child_element, acl, client)


@handler_for(ER_EMAIL, 'erItemTypeMailPage')
def handle_mail(element, parent_acl, client):
    """
    The mail file contains the mail message, and attachments. I have no idea
    how attachments work yet from the eRoom side, but using the separate
    document class in aodocs allows us to assign multiple attachments to a
    single document (a restriction applied to only the drive UI backed view)
    """

    path = eroom.findtext(element, './er:Path')
    uid = eroom.findtext(element, './er:ID')

    if client.has_imported_document(uid):
        return

    logger.info('Handling Mail Message %s:', path)

    acl = client.load_acl(uid)
    acl.remove_redundant_editors(parent_acl)

    name = eroom.findtext(element, './er:Name')
    path = eroom.findtext(element, './er:Path')
    sender = eroom.findtext(element, './er:Creator')
    description = eroom.findtext(element, './er:Description')
    description = '<br />'.join([escape(x) for x in description.split('\n')])
    creation_date = eroom.findtext(element, './er:CreateDate')

    attachments = client.list_eroom_items(uid)
    attachment_ids = []

    for attachment_el in attachments:
        # upload individual attachments for attaching to email.
        # aodocs do not need to be created for them.
        c_uid = eroom.findtext(attachment_el, './er:ID')
        c_title = eroom.findtext(attachment_el, './er:Name')
        c_ext = eroom.findtext(attachment_el, './er:Extension')
        c_path = client.download_eroom_item(
            c_uid, "%s%s" % (c_uid, c_ext))

        logger.info('Uploading attachment %s for Email %s' % (c_title, name))

        attachment_ids.append(
            client.upload_aodocs_file(c_uid,
                                      c_path,
                                      title=c_title))

    email = {
        'className': client.email_document_class,
        'title': name,
        'categories': {
            'Folder': [eroom.dirname(path)],
        },
        'richText': description,
        'attachments': attachment_ids,
        'creationDate': translate_timestamp(creation_date),
        'acl': aodocs.format_acl(acl.viewers, acl.editors),
        'applyContextACL': acl.should_inherit_acls,
        'ignoreMultiAttachmentCheck': True
    }

    if sender.strip():
        email['initialAuthor'] = sender

    client.create_aodocs_document(uid, email)
    client.record_imported_document(uid)


@handler_for(ER_DOCUMENT, 'erItemTypeFile')
def handle_file(element, parent_acl, client):

    # exit immediately if we're importing emails.
    if client.only_type and client.only_type != ER_DOCUMENT:
        return

    uid = eroom.findtext(element, './er:ID')

    if client.has_imported_document(uid):
        return

    acl = client.load_acl(uid)
    acl.remove_redundant_editors(parent_acl)

    name = eroom.findtext(element, './er:Name')
    path = eroom.findtext(element, './er:Path')
    extension = eroom.findtext(element, './er:Extension')
    creator = eroom.findtext(element, './er:Creator/er:Email')
    creation_date = eroom.findtext(element, './er:CreateDate')
    modifier = eroom.findtext(element, './er:Modifier/er:Email')
    modification_date = eroom.findtext(element, './er:ModifyDate')

    logger.info('Handling file %s:', path)

    filename = '%s%s' % (uid, extension)

    logger.info('Fetching item with ID %s from eRoom', uid)
    item_file_path = client.download_eroom_item(uid, filename)

    logger.info('Uploading %s (%s) to Google Drive', name, item_file_path)
    file_id = client.upload_aodocs_file(uid, item_file_path, title=name)

    document = {
        'className': client.document_class,
        'title': name,
        'categories': {
            'Folder': [eroom.dirname(path)],
        },
        'attachments': [file_id],
        'initialAuthor': creator,
        'creationDate': translate_timestamp(creation_date),
        'updateAuthor': modifier,
        'modificationDate': translate_timestamp(modification_date),
        'acl': aodocs.format_acl(acl.viewers, acl.editors),
        'applyContextACL': acl.should_inherit_acls,
    }

    client.create_aodocs_document(uid, document)
    client.record_imported_document(uid)


@handler_for(ER_DOCUMENT, 'erItemTypeFolderPage')
def handle_folder(element, parent_acl, client):
    uid = eroom.findtext(element, './er:ID')
    path = eroom.findtext(element, './er:Path')

    if client.has_imported_document(uid):
        logging.info("Skipping %s, already imported" % path)
        return

    acl = client.load_acl(uid)
    logger.info('Handling folder %s:', path)

    child_elements = client.list_eroom_items(uid)

    num_children = len(child_elements)
    children_count = 1
    for child_element in child_elements:
        run_handler(child_element, acl, client)
        logger.info('Processed child of %s - %s of %s' % (path,
                                                          children_count,
                                                          num_children))
        children_count += 1

    # Remove redundant ACLs from the parent AFTER processing all of
    # the children so that the children know that the parent has the
    # redundant ACLs, and they can be removed transitively.
    acl.remove_redundant_editors(parent_acl)

    folder = {
        'folderNames': eroom.get_path(path),
        'acl': aodocs.format_acl(acl.viewers, acl.editors),
        'applyContextACL': acl.should_inherit_acls,
    }

    client.create_aodocs_folder(uid, client.document_class, folder)

    # Folder Descriptions.
    handle_folder_description(element, uid, path, acl, client)

    client.record_imported_document(uid)
    logging.info("Folder %s Completed" % path)


def handle_folder_description(element, folder_uid, folder_path, acl, client):
    """Given that a folder has a description, we should create a new file
    inside the generated folder called "DESCRIPTION" with the HTML content
    of the description as the body of the file.
    """

    description = eroom.findtext(element, './er:Description')
    desc_uid = folder_uid + '_DESCRIPTION'

    if client.has_imported_document(desc_uid) or not description:
        return

    desc_document = {
        'className': client.document_class,
        'title': 'DESCRIPTION',
        'richText': description,
        'categories': {
            'Folder': [eroom.get_path(folder_path)],
        },
        'attachments': [],
        'initialAuthor': None,  # recorded as 'system'
        'creationDate': None,  # defaults to now()
        'updateAuthor': None,  # no updates
        'modificationDate': None,  # has never been modified
        'acl': aodocs.format_acl(acl.viewers, acl.editors),  # shared /w folder
        'applyContextACL': acl.should_inherit_acls,
    }

    client.create_aodocs_document(desc_uid, desc_document)


def compute_covering_acl(element, parent_acl, client):
    """Computes and stores ACL for an item. If this is a container item,
    then the ACLs of all child items are also computed, and any edit rights
    which are common at all children are promoted to the parent's edit
    scope.

    """

    uid = eroom.findtext(element, './er:ID')
    path = eroom.findtext(element, './er:Path')

    eroom_type = eroom.get_xsi_type(element)

    try:
        # attempt to pass on acls that have already been loaded
        existing_acl = client.load_acl(uid)
        if existing_acl:
            logger.info("ACL Loaded from cache for %s", path)
            return None
    except IOError:
        pass

    if get_handler(eroom_type) is None:
        return None

    is_container_string = eroom.findtext(element, './er:IsContainer')
    is_container = is_container_string.lower() == 'true'
    acl = acls.Acl.from_xml_element(parent_acl, element)

    if not is_container:
        client.store_acl(uid, acl)
        return acl

    logger.info("Preprocessing ACLs for %s ...", path)

    child_elements = client.list_eroom_items(uid)
    child_acls = []

    for child_element in child_elements:
        child_acl = compute_covering_acl(child_element, acl, client)
        child_acls.append(child_acl)

    # Edit access will be manually assigned by LBL to users that need to
    # be able to add files to a folder. To prevent edit access being inherited
    # inappropriately, we only assign edit access to users who have edit access
    # to all child items that inherit permissions from this item

    child_editors = [an_acl.edit_scope.editors
                     for an_acl in child_acls
                     if an_acl is not None and an_acl.should_inherit_acls]

    if len(child_editors) > 0:
        acl.editors = reduce(lambda a, b: a & b, child_editors)
    else:
        acl.editors = frozenset()

    client.store_acl(uid, acl)
    return acl


def compute_acls(client):
    """Compute and stores ACLs for all handled items recursively. Returns an
    ACL for the root folder based on the members of the eRoom.

    """

    logger.info("Fetching room member list ...")
    member_elements = client.list_eroom_members()
    member_emails = eroom.findtexts(member_elements, './/er:Email')

    # In order to propagate ACLs for explicit edit access to all items
    # imported into AODocs, the root ACL is view-only by all members of
    # the room. Edit access is left blank, as edit access is not inherited.

    logger.info("Preprocessing ACLs for root folder ...")
    root_acl = acls.Acl(acls.EveryoneScope(member_emails), acls.ListScope([]))
    elements = client.list_eroom_items(None)

    for element in elements:
        compute_covering_acl(element, root_acl, client)

    return root_acl


def upload_documents(root_acl, client):
    elements = client.list_eroom_items(None)

    total = len(elements)
    current = 1
    for element in elements:
        run_handler(element, root_acl, client)
        logger.info('Processed folder %d of %d' % (current, total))
        current += 1


def do_import(client):
    root_acl = compute_acls(client)
    upload_documents(root_acl, client)
