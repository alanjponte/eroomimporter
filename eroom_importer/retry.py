import functools
import logging
import random
import time


logger = logging.getLogger(__name__)


class FinalError(RuntimeError):
    pass


def retry(f):
    RETRIES = 5

    @functools.wraps(f)
    def decorated(*args, **kwargs):
        for attempt in range(1, RETRIES + 1):
            try:
                return f(*args, **kwargs)
            except (ValueError, TypeError, SyntaxError, FinalError):
                raise
            except Exception as e:
                around = 2 ** attempt
                timeout = random.randint(int(around / 2), int(around * 3 / 2))
                msg = "An unexpected exception occurred. Retrying in %d s\n%s"
                logger.exception(msg, timeout, str(e))
                time.sleep(timeout)

        logger.critical("Retries failed, giving up")
        raise e

    return decorated
