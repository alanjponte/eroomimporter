from datetime import datetime
import os
import os.path

from apiclient.discovery import build
from apiclient.http import MediaFileUpload
import httplib2
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.file import Storage


CLIENT_ID = '257580895482-0cqsshb2d6j39j9jato8sbbvec1nmka6.apps.googleusercontent.com'
CLIENT_SECRET = 'RtuJvZL9X3-DSR8PzB95yL9q'
OAUTH_SCOPE = 'https://www.googleapis.com/auth/drive'
REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'

CREDENTIAL_FILENAME = 'oauth-credentials.dat'


def get_drive_service():
    storage = Storage(CREDENTIAL_FILENAME)
    credentials = storage.get()

    if credentials is None:
        flow = OAuth2WebServerFlow(CLIENT_ID, CLIENT_SECRET, OAUTH_SCOPE, REDIRECT_URI)
        authorize_url = flow.step1_get_authorize_url()
        print 'Go to the following link in your browser: ' + authorize_url
        code = raw_input('Enter verification code: ').strip()
        credentials = flow.step2_exchange(code)
        storage.locked_put(credentials)

    credentials.set_store(storage)
    http = httplib2.Http()
    http = credentials.authorize(http)
    service = build('drive', 'v2', http=http)
    return service


def upload_file(service, path):
    media_body = MediaFileUpload(path, mimetype='application/octet-stream', resumable=True)
    body = {
       'title': os.path.basename(path),
       'mimeType': 'application/octet-stream',
    }

    file = service.files().insert(body=body, media_body=media_body).execute()


if __name__ == '__main__':
    service = get_drive_service()

    for filename in os.listdir('files'):
        path = os.path.join('files', filename)
        start_time = datetime.now()
        upload_file(service, path)
        file_size = os.path.getsize(path)
        upload_time = datetime.now() - start_time
        upload_seconds = upload_time.total_seconds()
        context = {
            'path': path,
            'size': file_size,
            'seconds': upload_seconds,
            'speed': file_size / 1024 / upload_seconds,
        }

        print '{path}: uploaded {size} bytes in {seconds} seconds ({speed:.2f} KB/s)'.format(**context)
