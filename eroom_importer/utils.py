import os
import os.path


def ensure_directory_exists(path):
    dirname = os.path.dirname(path)

    if dirname == '':
        return

    if not os.path.exists(dirname):
        os.makedirs(dirname)


def excerpt(s):
    content = s[:4096]
    length_omitted = len(s) - 4096

    if length_omitted > 0:
        return '%s\n ... plus %d more characters' % (content, length_omitted)
    else:
        return content


class AppendableMap:
    def __init__(self, path):
        self.mapping = {}
        self.load_from_file(path)  # Positions at EOF by reading all content
        self.file_handle = open(path, 'a')

    def close(self):
        self.file_handle.close()

    def __contains__(self, key):
        return key.strip() in self.mapping

    def __getitem__(self, key):
        return self.mapping[key.strip()]

    def __setitem__(self, key, value):
        key = key.strip()
        value = value.strip()
        self.mapping[key] = value
        self.write_pair(key, value)

    def write_pair(self, key, value):
        self.file_handle.write('%s\n%s\n' % (key, value))
        self.file_handle.flush()

    def load_from_file(self, path):
        if not os.path.exists(path):
            return

        with open(path, 'r') as f:
            lines = f.readlines()

        for i in range(0, len(lines), 2):
            key = lines[i].strip()
            value = lines[i + 1].strip()

            self.mapping[key] = value
