from distutils.core import setup
#import py2exe
import py2app

setup(
    name='eroom-importer',
    version='1.0',
    console=['eroom-importer.py'],
    options={
        'py2exe': {
            'bundle_files': 1,
            'includes': ['lxml.etree', 'lxml._elementpath', 'gzip'],
        }
    },
    zipfile=None,
)
