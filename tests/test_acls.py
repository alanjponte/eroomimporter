from eroom_importer.eroom import er
from eroom_importer import mediator


def acl_to_tuple_set(acl):
    """Convert an AODocs-style ACL list of dicts into a list of
    `(email, right)` pairs. This makes it easier to compare actual ACLs
    to expected values.

    """
    return set((entry['value'], entry['right']) for entry in acl)


def emails_to_members(emails):
    """Convert a list of email addresses to a list of er:Member XML
    elements.

    """
    return [er.Member(er.Email(email)) for email in emails]


def erItemTypeFile(uid=None, open_access_scope=None, open_access_list=(),
                   edit_access_scope=None, edit_access_list=()):
    """Construct an er:Item element of type erItemTypeFile with the
    specified item ID and permission scopes. The scope types should be one
    of `erScopeAnyone`, `erScopeList`, or `erScopeCoordinatorsOnly`. A scope
    list should be a list of email addresses to be included in the ACL.

    """

    open_list = emails_to_members(open_access_list)
    edit_list = emails_to_members(edit_access_list)

    xml_element = er.Item(
        er.ID(uid),
        er.Path('/dont/use/this'),
        er.IsContainer('False'),
        er.Extension('.xxx'),
        er.Creator('brendan.macdonell@sheepdog.com'),
        er.CreateDate('2013-07-25T19:39:00-03:00'),
        er.Modifier('brendan.macdonell@sheepdog.com'),
        er.ModifyDate('2013-07-25T19:40:05-03:00'),
        er.AccessControl(
            er.OpenAccessScope(open_access_scope),
            er.OpenAccessList(*open_list),
            er.EditAccessScope(edit_access_scope),
            er.EditAccessList(*edit_list),
        ),
        **{'{http://www.w3.org/2001/XMLSchema-instance}type': 'erItemTypeFile'}
    )

    return xml_element


def erItemTypeFolderPage(uid=None, open_access_scope=None, open_access_list=(),
                         edit_access_scope=None, edit_access_list=()):
    """Construct an er:Item element of type erItemTypeFolderPage with the
    specified item ID and permission scopes. The scope types should be one
    of `erScopeAnyone`, `erScopeList`, or `erScopeCoordinatorsOnly`. A scope
    list should be a list of email addresses to be included in the ACL.

    """

    open_list = emails_to_members(open_access_list)
    edit_list = emails_to_members(edit_access_list)

    xml_element = er.Item(
        er.ID(uid),
        er.Path('/dont/use/this'),
        er.IsContainer('True'),
        er.AccessControl(
            er.OpenAccessScope(open_access_scope),
            er.OpenAccessList(*open_list),
            er.EditAccessScope(edit_access_scope),
            er.EditAccessList(*edit_list),
        ),
        **{'{http://www.w3.org/2001/XMLSchema-instance}type': 'erItemTypeFolderPage'}
    )

    return xml_element


class StubImportClient(object):
    """A StubImportClient is a test stub for a mediator.ImportClient that
    can be supplied with a list of email addresses of members of a room,
    and which items should be returned when the child items of a given
    parent item ID are requested. The stub keeps track of all acls persisted,
    as well as all documents and folders sent to AODocs.

    """

    def __init__(self, members=None, item_child_map=None):
        """Construct a new StubImportClient representing a room with a
        specified set of members and items.

        :param members: A list of email addresses of members of the room
        :param item_child_map: A dict mapping from string representing item
            IDs of container items to an `er:Items` element (as an
            `etree.Element`) containing the child items in the container

        """

        self.document_class = 'Document'

        self.acls = {}
        self.documents = {}
        self.folders = {}
        self.members = er.Members(*emails_to_members(members))
        self.item_child_map = item_child_map

    def close(self):
        pass

    def cleanup(self):
        pass

    def list_eroom_members(self):
        return self.members

    def list_eroom_items(self, parent_item_id=None):
        return self.item_child_map[parent_item_id]

    def download_eroom_item(self, item_id, filename):
        return None

    def upload_aodocs_file(self, item_id, filename, title=None):
        return None

    def create_aodocs_document(self, item_id, document):
        self.documents[item_id] = document
        return None

    def create_aodocs_folder(self, item_id, class_name, folder):
        self.folders[item_id] = folder
        return None

    def has_imported_document(self, item_id):
        return False

    def record_imported_document(self, item_id):
        pass

    def store_acl(self, uid, acl):
        self.acls[uid] = acl

    def load_acl(self, uid):
        return self.acls[uid]


def test_file_view_anyone_edit_anyone():
    """If a file item has 'everyone' as viewers and editors, then
    all inherited ACLs should be copied as editors of the file,
    and the file should inherit ACLs from the parent.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeAnyone',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.documents['0_1']['acl']) == set([
        ('u1@test.ca', 'WRITE'),
        ('u2@test.ca', 'WRITE'),
    ])

    assert client.documents['0_1']['applyContextACL'] is True


def test_file_view_anyone_edit_list():
    """If a file item has 'everyone' as viewers, and an explicit list of
    users as editors, then the editors should be editors of the file, and
    the file should inherit ACLs from the parent.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca'],
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.documents['0_1']['acl']) == set([
        ('u2@test.ca', 'WRITE'),
    ])

    assert client.documents['0_1']['applyContextACL'] is True


def test_file_view_anyone_edit_coordinators():
    """If a file item has 'everyone' as viewers, and only coordinators
    as editors, then the editors list should be empty and the file should
    inherit ACLs from the parent.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert client.documents['0_1']['acl'] == []
    assert client.documents['0_1']['applyContextACL'] is True


def test_file_view_list_edit_anyone():
    """If the viewers for an item are explicitly listed, and the editors
    for an item are 'everyone', then the listed viewers should be copied
    to the list of editors. Permissions should not be inherited.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca'],
                    edit_access_scope='erScopeAnyone',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.documents['0_1']['acl']) == set([
        ('u1@test.ca', 'WRITE'),
    ])

    assert client.documents['0_1']['applyContextACL'] is False


def test_file_view_list_edit_list():
    """If both the viewers and editors of a file are explicitly listed,
    then both lists should be copied to the file's ACL. Permissions should
    not be inherited.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca', 'u2@test.ca'],
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca'],
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.documents['0_1']['acl']) == set([
        ('u1@test.ca', 'READ'),
        ('u2@test.ca', 'WRITE'),
    ])

    assert client.documents['0_1']['applyContextACL'] is False


def test_file_view_list_edit_coordinators():
    """If the viewers of a file are explicitly listed, and only coordinators
    are editors, then the viewers should be copied to the file's ACL,
    then both lists should be copied to the file's ACL. Permissions should
    not be inherited.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca', 'u2@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.documents['0_1']['acl']) == set([
        ('u1@test.ca', 'READ'),
        ('u2@test.ca', 'READ'),
    ])

    assert client.documents['0_1']['applyContextACL'] is False


def test_file_view_coordinators_edit_discarded():
    """If only coordinators can view a file, then editors should be discarded
    no matter what (as coordinators can edit any file, restricting the list
    will not have any effect.)

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeCoordinatorsOnly',
                    edit_access_scope='erScopeAnyone',
                ),
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeCoordinatorsOnly',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca'],
                ),
                erItemTypeFile(
                    uid='0_3',
                    open_access_scope='erScopeCoordinatorsOnly',
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert client.documents['0_1']['acl'] == []
    assert client.documents['0_1']['applyContextACL'] is False

    assert client.documents['0_2']['acl'] == []
    assert client.documents['0_2']['applyContextACL'] is False

    assert client.documents['0_3']['acl'] == []
    assert client.documents['0_3']['applyContextACL'] is False


def test_view_coordinator_only_nullifies_child_acls():
    """If a folder is only viewable by coordinators, then explicit lists
    of users in ACLs on children should be empty, even if they do not
    inherit from the folder.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeCoordinatorsOnly',
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca'],
                ),
                erItemTypeFile(
                    uid='0_3',
                    open_access_scope='erScopeList',
                    open_access_list=['u2@test.ca'],
                    edit_access_scope='erScopeAnyone',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert client.documents['0_2']['acl'] == []
    assert client.documents['0_2']['applyContextACL'] is True

    assert client.documents['0_3']['acl'] == []
    assert client.documents['0_3']['applyContextACL'] is False


def test_folder_editors_are_discarded():
    """If a folder has editors set on it, either implicitly or explicitly,
    then those ACLs should be discarded. This gives us more flexibility in
    inheriting view access than we would otherwise have. LBL has taken
    responsibility for adding back folder-level edit access and adjusting
    inherited permissions if necessary.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca'],
                ),
                erItemTypeFolderPage(
                    uid='0_2',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeAnyone',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_3',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_2': er.Items(
                erItemTypeFile(
                    uid='0_4',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert client.folders['0_1']['acl'] == []

    assert client.folders['0_2']['acl'] == []

    assert client.documents['0_3']['acl'] == []
    assert client.documents['0_3']['applyContextACL'] is True

    assert client.documents['0_4']['acl'] == []
    assert client.documents['0_4']['applyContextACL'] is True


def test_viewer_must_be_member():
    """If an item has explicit view permissions set, then only the viewers
    who are also members of the room should be kept in the ACL. Other viewers
    who would not have access in eRoom should be excluded.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u3@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert client.documents['0_1']['acl'] == []
    assert client.documents['0_1']['applyContextACL'] is False


def test_viewer_must_be_parent_viewer():
    """If an item has explicit view permissions set, then only the viewers
    who are also viewers of the parent should be kept in the ACL. Other viewers
    who would not have access in eRoom should be excluded.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u2@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert client.documents['0_2']['acl'] == []
    assert client.documents['0_2']['applyContextACL'] is False


def test_editor_must_be_viewer():
    """If an item has explicit edit permissions set, then only the editors who
    are also viewers of the item should be kept in the ACL. Other editors
    should be excluded.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca'],
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca'],
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.documents['0_1']['acl']) == set([
        ('u1@test.ca', 'READ'),
    ])
    assert client.documents['0_1']['applyContextACL'] is False


def test_editor_need_not_be_parent_editor():
    """If an item has explicit edit permissions set, then those editors who are
    not also editors of the parent item should still remain as editors in the
    ACL sent to AODocs.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca'],
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeList',  # Prevents edit promotion
                    open_access_list=['u2@test.ca'],
                    edit_access_scope='erScopeAnyone',  # ie. u2@test.ca
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.documents['0_2']['acl']) == set([
        ('u2@test.ca', 'WRITE'),
    ])
    assert client.documents['0_2']['applyContextACL'] is False


def test_viewers_inherited_from_room():
    """If the viewers for an item are 'everyone', and the item is in the root
    folder of a room, then the item should inherit the ACL from the room.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=()
                ),
            ),
        }
    )

    mediator.do_import(client)

    assert client.documents['0_1']['acl'] == []
    assert client.documents['0_1']['applyContextACL'] is True


def test_viewers_inherited_from_folder():
    """If the viewers for an item are 'everyone', and the item is in a folder,
    then the item should inherit the ACL from the folder.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca'],
                    edit_access_scope='erScopeAnyone',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeAnyone',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert client.documents['0_2']['acl'] == []
    assert client.documents['0_2']['applyContextACL'] is True


def test_subsetted_viewers_inherited():
    """If the viewers for an item are 'everyone', and the item is in a folder
    that itself has an explicit list of viewers, then the parent's list of
    viewers should be inherited.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert client.documents['0_2']['acl'] == []
    assert client.documents['0_2']['applyContextACL'] is True


def test_subsetted_viewers_subsetted():
    """If the viewers on an item are an explicit list, and the item is in a
    folder which itself has an explicit list of viewers, then the parent's
    permissions should not be inherited.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca', 'u2@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeList',
                    open_access_list=['u2@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.documents['0_2']['acl']) == set([
        ('u2@test.ca', 'READ'),
    ])
    assert client.documents['0_2']['applyContextACL'] is False


def test_edit_not_promoted_without_consensus():
    """If there are no users who have edit access to all items in the set of
    items that inherit permissions from a folder, then no edit access should
    be promoted to the folder.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca', 'u2@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca'],
                ),
                erItemTypeFile(
                    uid='0_3',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca'],
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.folders['0_1']['acl']) == set([
        ('u1@test.ca', 'READ'),
        ('u2@test.ca', 'READ'),
    ])
    assert client.folders['0_1']['applyContextACL'] is False

    assert acl_to_tuple_set(client.documents['0_2']['acl']) == set([
        ('u1@test.ca', 'WRITE'),
    ])
    assert client.documents['0_2']['applyContextACL'] is True

    assert acl_to_tuple_set(client.documents['0_3']['acl']) == set([
        ('u2@test.ca', 'WRITE'),
    ])
    assert client.documents['0_3']['applyContextACL'] is True


def test_edit_is_promoted_to_folder():
    """If there are users who have edit access to all items in the set of
    items that inherit permissions from a folder, then those users should be
    given edit access to the folder.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca', 'u3@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca', 'u2@test.ca'],
                ),
                erItemTypeFile(
                    uid='0_3',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca', 'u3@test.ca'],
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.folders['0_1']['acl']) == set([
        ('u2@test.ca', 'WRITE'),
    ])
    assert client.folders['0_1']['applyContextACL'] is True


def test_edit_not_promoted_when_view_is_overridden():
    """If a mid-level folder in a hierarchy with common edit permissions
    has explicit view permissions set, then the edit permission should not
    be promoted above the break in the inheritance chain.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca', 'u3@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca'],
                ),
                erItemTypeFolderPage(
                    uid='0_3',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca', 'u2@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_3': er.Items(
                erItemTypeFile(
                    uid='0_4',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca'],
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.folders['0_3']['acl']) == set([
        ('u1@test.ca', 'WRITE'),
        ('u2@test.ca', 'READ'),
    ])
    assert client.folders['0_3']['applyContextACL'] is False

    assert client.documents['0_4']['acl'] == []
    assert client.documents['0_4']['applyContextACL'] is True


def test_edit_promoted_when_sibling_view_is_overridden():
    """If a hierarchy of items have common edit permissions, except for some
    number of items which also happen to have explicitly set view
    permissions, then the edit permissions in the rest of the hierarchy
    should be promoted anyway (as they won't be inherited inappropriately
    by the items with explicit view permissions.)

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca', 'u3@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca'],
                    edit_access_scope='erScopeAnyone',
                ),
                erItemTypeFile(
                    uid='0_3',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeAnyone',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.folders['0_1']['acl']) == set([
        ('u1@test.ca', 'WRITE'),
        ('u2@test.ca', 'WRITE'),
        ('u3@test.ca', 'WRITE'),
    ])

    assert acl_to_tuple_set(client.documents['0_2']['acl']) == set([
        ('u1@test.ca', 'WRITE'),
    ])
    assert client.documents['0_2']['applyContextACL'] is False

    assert client.documents['0_3']['acl'] == []
    assert client.documents['0_3']['applyContextACL'] is True


def test_edit_promotion_doesnt_displace_viewer_list():
    """If there are users whose edit access is promoted to the parent item,
    then the parent item should still have explicit ACLs for the viewers who
    are not also editors.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca', 'u3@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=[
                        'u1@test.ca',
                        'u2@test.ca',
                        'u3@test.ca',
                    ],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca', 'u2@test.ca'],
                ),
                erItemTypeFile(
                    uid='0_3',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca', 'u3@test.ca'],
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.folders['0_1']['acl']) == set([
        ('u1@test.ca', 'READ'),
        ('u2@test.ca', 'WRITE'),
        ('u3@test.ca', 'READ'),
    ])
    assert client.folders['0_1']['applyContextACL'] is False


def test_promoted_editors_are_removed_from_children():
    """If users' edit permissions are promoted to a parent, then they should
    be removed from all descendants (not just children.)

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca', 'u3@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca', 'u2@test.ca'],
                ),
                erItemTypeFile(
                    uid='0_3',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca', 'u3@test.ca'],
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.documents['0_2']['acl']) == set([
        ('u1@test.ca', 'WRITE'),
    ])
    assert client.documents['0_2']['applyContextACL'] is True

    assert acl_to_tuple_set(client.documents['0_3']['acl']) == set([
        ('u3@test.ca', 'WRITE'),
    ])
    assert client.documents['0_3']['applyContextACL'] is True


def test_promoted_editors_span_multiple_levels():
    """If users edit permissions are promoted, they should be promoted to
    the highest-level in the folder tree for which all inheriting descendants
    have those edit permissions. The edit permissions should be removed from
    all of the intervening levels.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca', 'u3@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=[
                        'u1@test.ca',
                        'u2@test.ca',
                        'u3@test.ca',
                    ],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca', 'u2@test.ca'],
                ),
                erItemTypeFolderPage(
                    uid='0_3',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca', 'u3@test.ca'],
                ),
            ),
            '0_3': er.Items(
                erItemTypeFile(
                    uid='0_4',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca'],
                ),
                erItemTypeFolderPage(
                    uid='0_5',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca', 'u3@test.ca'],
                ),
            ),
            '0_5': er.Items(
                erItemTypeFile(
                    uid='0_6',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u2@test.ca', 'u3@test.ca'],
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.folders['0_1']['acl']) == set([
        ('u1@test.ca', 'READ'),
        ('u2@test.ca', 'WRITE'),
        ('u3@test.ca', 'READ'),
    ])
    assert client.folders['0_1']['applyContextACL'] is False

    assert acl_to_tuple_set(client.documents['0_2']['acl']) == set([
        ('u1@test.ca', 'WRITE'),
    ])
    assert client.documents['0_2']['applyContextACL'] is True

    assert client.folders['0_3']['acl'] == []
    assert client.folders['0_3']['applyContextACL'] is True

    assert client.documents['0_4']['acl'] == []
    assert client.documents['0_4']['applyContextACL'] is True

    assert acl_to_tuple_set(client.folders['0_5']['acl']) == set([
        ('u3@test.ca', 'WRITE'),
    ])
    assert client.folders['0_5']['applyContextACL'] is True

    assert client.documents['0_6']['acl'] == []
    assert client.documents['0_6']['applyContextACL'] is True


def test_edit_is_not_promoted_to_room():
    """The end user running the importer will not know which users have been
    given edit permission at the library level -- instead, they will manually
    add viewers to the library. As such, we can't promote edit permissions to
    the room level as this assumption will not be communicated to the end user.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFile(
                    uid='0_1',
                    open_access_scope='erScopeAnyone',
                    edit_access_scope='erScopeList',
                    edit_access_list=['u1@test.ca'],
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert acl_to_tuple_set(client.documents['0_1']['acl']) == set([
        ('u1@test.ca', 'WRITE'),
    ])
    assert client.documents['0_1']['applyContextACL'] is True


def test_inheritance_is_not_inferred():
    """If an item has explicit view permissions set, it should not inherit
    permissions from the parent, even if the parent has a matching set of
    permissions. Though this might seem like a way to cut down on complexity,
    it will cause permission hierarchies to be merged that should not be.

    For example, just because a common set of users have access to a budgeting
    and an internal affairs subfolder of a shared parent, this does not mean
    that all users with access to the shared parent in the future should be
    able to access both subfolders.

    """

    client = StubImportClient(
        members=('u1@test.ca', 'u2@test.ca'),
        item_child_map={
            None: er.Items(
                erItemTypeFolderPage(
                    uid='0_1',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
            '0_1': er.Items(
                erItemTypeFile(
                    uid='0_2',
                    open_access_scope='erScopeList',
                    open_access_list=['u1@test.ca'],
                    edit_access_scope='erScopeCoordinatorsOnly',
                ),
            ),
        },
    )

    mediator.do_import(client)

    assert client.documents['0_2']['applyContextACL'] is False
