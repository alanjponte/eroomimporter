from contextlib import closing

from eroom_importer.utils import AppendableMap


MAPPINGS = (
    ('k', 'v'),
    ('with spaces', 'works just as well'),
    ('something else=:', 'not a problem'),
    ('SA9120 !', '!!!NO HABLO C#!!!'),
    ('more example text that is   quite long', 'a value'),
)


def test_mapping(tmpdir):
    """An AppendableMap should be able to retrieve keys and values that
    have been placed in it since it was loaded from disk.

    """

    path = str(tmpdir.join('persistent.map'))

    with closing(AppendableMap(path)) as appendable_map:
        for k, v in MAPPINGS:
            appendable_map[k] = v

        for k, v in MAPPINGS:
            assert appendable_map[k] == v


def test_persistence(tmpdir):
    """An AppendableMap should preserve all of its mappings when it is
    written to disk and reloaded.

    """

    path = str(tmpdir.join('persistent.map'))

    with closing(AppendableMap(path)) as appendable_map:
        for k, v in MAPPINGS:
            appendable_map[k] = v

    with closing(AppendableMap(path)) as appendable_map:
        for k, v in MAPPINGS:
            assert appendable_map[k] == v


def test_rewriting(tmpdir):
    """An AppendableMap should preserve mappings across multiple loads.
    Reloading an AppendableMap and adding data to it should not cause
    any previously-written data to be lost.

    """

    INITIAL_VALUES = MAPPINGS[:3]
    ADDED_VALUES = MAPPINGS[3:]

    path = str(tmpdir.join('persistent.map'))

    with closing(AppendableMap(path)) as appendable_map:
        for k, v in INITIAL_VALUES:
            appendable_map[k] = v

    with closing(AppendableMap(path)) as appendable_map:
        for k, v in ADDED_VALUES:
            appendable_map[k] = v

    with closing(AppendableMap(path)) as appendable_map:
        for k, v in INITIAL_VALUES + ADDED_VALUES:
            assert appendable_map[k] == v


INITIAL_VALUES = (
    ('shopping list', 'salsa and hamburger'),
    ('akeywithnospaces', 'avaluewithnoneeither'),
    ('maximum water level (cm)', '100'),
    ('a key which is not rewritten', 'its original value'),
)

REPLACEMENT_VALUES = (
    ('shopping list', 'pho sho'),
    ('maximum water level (cm)', '45 above my basement floor'),
)

EXPECTED_VALUES = dict(INITIAL_VALUES)
EXPECTED_VALUES.update(dict(REPLACEMENT_VALUES))


def test_replacement(tmpdir):
    """An AppendableMap should be updateable. Adding a value for a key
    that already exists in an AppendableMap should replace the original
    value.

    """

    path = str(tmpdir.join('persistent.map'))

    with closing(AppendableMap(path)) as appendable_map:
        for k, v in INITIAL_VALUES:
            appendable_map[k] = v

        for k, v in REPLACEMENT_VALUES:
            appendable_map[k] = v

        for k, v in EXPECTED_VALUES.items():
            assert appendable_map[k] == v


def test_replacement_persistence(tmpdir):
    """An AppendableMap should be updateable. Adding a value for a key
    that already exists in an AppendableMap should replace the original
    value. The replacements should persist if the AppendableMap is reloaded
    from disk.

    """

    path = str(tmpdir.join('persistent.map'))

    with closing(AppendableMap(path)) as appendable_map:
        for k, v in INITIAL_VALUES:
            appendable_map[k] = v

    with closing(AppendableMap(path)) as appendable_map:
        for k, v in REPLACEMENT_VALUES:
            appendable_map[k] = v

    with closing(AppendableMap(path)) as appendable_map:
        for k, v in EXPECTED_VALUES.items():
            assert appendable_map[k] == v
