from eroom_importer import eroom


def test_strips_leading_slash():
    """eRoom paths have a leading slash which should be removed in AODocs."""
    parts = eroom.get_path('/root')
    assert parts == ['root']


def test_splits_on_slashes():
    """eRoom paths are written as forward-slash separated components."""
    parts = eroom.get_path('/root/sub')
    assert parts == ['root', 'sub']


def test_doesnt_split_on_escaped_slashes():
    """If a forward-slash is escaped be a preceding backslash, then the path
    should not be split on the escaped slash.

    """

    parts = eroom.get_path(r'/a \/ b')
    assert len(parts) == 1


def test_removes_escapes():
    """If a forward-slash is escaped be a preceding backslash, then the path
    backslash should be removed from the path in AODocs.

    """

    parts = eroom.get_path(r'/a \/ b')
    assert parts == ['a / b']


def test_preserves_escaped_backslashes():
    """If a backslash is escaped by another backslash, then only a single
    backslash should appear in the path in AODocs.

    """

    parts = eroom.get_path(r'/a \\ b')
    assert parts == [r'a \ b']
